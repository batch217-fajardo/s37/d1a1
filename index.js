
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();

// db connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.w87b5cj.mongodb.net/Course-Booking?retryWrites=true&w=majority", {
	useNewUrlParser: true, 
	useUnifiedTopology: true
});
// prompts message in the terminal once connection is open:
mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"));

// middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
})
// process.env port - working website link